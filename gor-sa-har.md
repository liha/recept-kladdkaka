# Gör så här

Sätt ugnen på 175°.

1. Smält smöret i en kastrull. Lyft av kastrullen från plattan.
2. Rör ner socker och ägg, blanda väl. Rör ner övriga ingredienser så att allt blir väl blandat.
3. Häll smeten i en smord och bröad form med löstagbar kant, ca 24 cm i diameter.
4. Grädda mitt i ugnen ca 15 min. Kakan blir låg med ganska hård yta och lite kladdig i mitten.
5. Låt kakan kallna. Pudra över florsocker. Servera med grädde eller glass och frukt.



