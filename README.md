# Kladdkaka

Recept på supergod och kladdig kladdkaka. Den är snabb och enkel att göra eftersom du rör ihop smeten direkt i kastrullen. Njut med vispad grädde eller glass.

![](/assets/aneta-pawlik-1248140-unsplash.jpg)



